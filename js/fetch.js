function fetchWind() {
    
    const baseUrl = "https://api.openweathermap.org/data/2.5/weather?";
    const units = 'metric';
    const apiKey = "31e0f7e873a94e22b6d381f5a7f48978";
    const cities = [null, "Helsinki", "Turku", "Kemiö", "Hanko", "Pori", "Rauma", "Vaasa", "Kalajoki", "Oulu"];

    function getWind() {
        
        let endpoint = "";
        document.getElementById("header").innerHTML = "<h2>Is there kitesurfable wind?</h2>";
        for (let i = 1; i < cities.length; i++) {
            endpoint = baseUrl + 
                'q=' + cities[i] +
                '&units=' + units + 
                '&appid=' + apiKey;
            fetch(endpoint).then((output) => {
                if (output.ok) {
                    return output.json();
                } else {
                    throw new Error("Something went wrong.");
                }
            })
            .then((data) => {
                showWind(data, i);
            })
            .catch((error) => {
                showError(error);
            });
        }
    }

    function showWind(data, i) {
        
        let output = "<p>Location: " + data.name + "</p>";
        output += "wind speed: " + data.wind.speed + " bft</p>";
        output += "wind direction: " + data.wind.deg + "&#176;<p></p>";

        document.getElementById("grid-item-" + i).innerHTML = output;
    }

    function showError(error) {
        
        document.getElementById("error").innerHTML = "<p>" + error + "</p>";
    }

    getWind();

}